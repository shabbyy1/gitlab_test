package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverflowDao {
    void saveOverflowList(OverflowList overflowList);

    Integer getOverflowListIdByNumber(String damageNumber);

    void saveOverflowListGoods(OverflowListGoods overflowListGoods);
    //报溢单查询
    List<OverflowList> list(String sTime, String eTime);
    //报溢单商品信息
    List<OverflowListGoods> goodsList(Integer overflowListId);
}
