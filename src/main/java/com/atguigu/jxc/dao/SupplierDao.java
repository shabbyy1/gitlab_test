package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;

//供应商
public interface SupplierDao {

    //供应商信息分页展示
    List<Supplier> getSupplierList(int offSet, Integer rows, String supplierName);
    //根据角色名称查找角色
    Supplier findSupplierByName(String supplierName);
    //新增供应商
    void insertSupplier(Supplier supplier);
    //修改供应商
    void updateSupplier(Supplier supplier);
    //删除供应商（支持批量删除）
    void deleteBySupplierId(String ids);

}
