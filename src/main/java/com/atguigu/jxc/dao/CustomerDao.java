package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Goods;

import java.util.List;

//客户管理
public interface CustomerDao {
    //客户列表分页（名称模糊查询）
    List<Goods> getCustomerList(int offSet, Integer rows, String customerName);
    //根据角色名称查找角色
    Customer findCustomerByName(String customerName);
    //新增角色
    void insertCustomer(Customer customer);
    //修改角色
    void updateCustomer(Customer customer);
    //批量删除角色
    void deleteByCustomerId(String deleteId);
}
