package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    //根据codeOrName或goodsTypeId模糊分页查询角色列表
    List<Goods> getGoodsList(int offSet, Integer rows, String codeOrName, Integer goodsTypeId);
    //分页查询商品信息
    List<Goods> listGoods(int offSet, Integer rows, String goodsName, Integer goodsTypeId);
    // 根据角色名称查找角色
    Goods findGoodsByName(String goodsName);
    //新增
    void insertGoods(Goods goods);
    //更新
    void updateGoods(Goods goods);
    //根据id查商品状态
    Integer getStateByGoodsId(Integer goodsId);
    //商品删除
    void delete(Integer goodsId);
    //分页查询无库存商品信息
    List<Goods> getNoInventoryQuantity(int offSet, Integer rows, String nameOrCode);
    //分页查询有库存商品信息
    List<Goods> getHasInventoryQuantity(int offSet, Integer rows, String nameOrCode);
    ////添加商品期初库存
    void saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);
    //删除商品库存
    void deleteStock(Integer goodsId);
    //根据id查商品
    Goods getGoodsByGoodsId(Integer goodsId);
    //查询所有 当前库存量 小于 库存下限的商品信息
    List<Goods> listAlarm();

}
