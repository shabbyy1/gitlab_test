package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

public interface DamageDao {
    //保存t_damage_list
    void saveDamageList(DamageList damageList);
    //保存t_damage_list_goods
    void saveDamageListGoods(DamageListGoods damageListGoods);
    //保存报损单
    Integer getDamageListIdByNumber(String damageNumber);
    //报损单查询
    List<DamageList> list(String sTime, String eTime);
    //查询报损单商品信息
    List<DamageListGoods> goodsList(Integer damageListId);
}
