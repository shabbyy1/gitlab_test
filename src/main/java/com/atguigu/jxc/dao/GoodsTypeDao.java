package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {



    Integer updateGoodsTypeState(GoodsType parentGoodsType);
    //新增商品分类1
    void save1(String goodsTypeName, Integer pId);
    //新增商品分类2
    void save2(String goodsTypeName, Integer pId);
    //删除商品分类
    void delete(Integer goodsTypeId);
    //三级分类
    List<GoodsType> loadGoodsType();

}
