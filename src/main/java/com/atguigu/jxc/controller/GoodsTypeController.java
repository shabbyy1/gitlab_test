package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//商品三级分类 todo
@RestController
@RequestMapping("goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    @PostMapping("save")//新增商品分类
    public ServiceVO save(String  goodsTypeName,Integer  pId) {
        return goodsTypeService.save(goodsTypeName,pId);
    }

    @PostMapping("delete")//删除商品分类
    public ServiceVO delete(Integer  goodsTypeId) {
        return goodsTypeService.delete(goodsTypeId);
    }

    @PostMapping("loadGoodsType")//三级分类
    public String loadGoodsType() {
        return goodsTypeService.loadGoodsType();
    }
}
