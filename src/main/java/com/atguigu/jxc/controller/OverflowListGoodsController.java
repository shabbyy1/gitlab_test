package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

//商品报溢
@RestController
@RequestMapping("overflowListGoods")
public class OverflowListGoodsController {

    @Autowired(required = false)
    private OverflowService overflowService;

    @PostMapping("save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {
        return overflowService.save(overflowList, overflowListGoodsStr);
    }

    @PostMapping("list")//报溢单查询
    public Map<String, Object> list(String  sTime, String  eTime) {
        return overflowService.list(sTime, eTime);
    }

    @PostMapping("goodsList")//查询报溢单商品信息
    public Map<String, Object> goodsList(Integer overflowListId) {
        return overflowService.goodsList(overflowListId);
    }
}
