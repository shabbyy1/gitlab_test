package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("customer")
public class CustomerController {

    @Autowired(required = false)
    private CustomerService customerService;

    @PostMapping("list")//客户列表分页（名称模糊查询）
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        return customerService.list(page, rows, customerName);
    }

    //供应商添加或修改
    @PostMapping("save")
    public ServiceVO save(Customer customer) {
        return customerService.save(customer);
    }

    //删除供应商（支持批量删除）
    @PostMapping("delete")
    public ServiceVO delete(String ids ){
        return customerService.delete(ids);
    }


}
