package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

//报损单
@RestController
@RequestMapping("damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageService damageService;

    @PostMapping("save")//保存
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        return damageService.save(damageList,damageListGoodsStr);
    }

    @PostMapping("list")//报损单查询
    public Map<String, Object> list(String  sTime, String  eTime) {
        return damageService.list(sTime, eTime);
    }

    @PostMapping("goodsList")//查询报损单商品信息
    public Map<String, Object> goodsList(Integer damageListId) {
        return damageService.goodsList(damageListId);
    }
}
