package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

//供应商管理
@RestController
@RequestMapping("supplier")
public class SupplierController {

    @Autowired(required = false)
    private SupplierService supplierService;


    //分页查询供应商
    @PostMapping("list")
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        return supplierService.list(page, rows, supplierName);
    }

    //供应商添加或修改
    @PostMapping("save")
    public ServiceVO save(  Supplier supplier) {
       return supplierService.save(supplier);
    }

    //删除供应商（支持批量删除）
    @PostMapping("delete")
    public ServiceVO delete(String ids ){
        return supplierService.delete(ids);
    }
}


