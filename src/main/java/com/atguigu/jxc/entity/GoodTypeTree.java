package com.atguigu.jxc.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
public class GoodTypeTree {
    private Integer id;
    private String text;
    private String state;
    private String iconCls;
    private Map<String,Object> attributes;
    private List<GoodTypeTree> children;
    private Integer pId;
}
