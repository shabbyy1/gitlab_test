package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import java.util.Map;


public interface OverflowService {
    ServiceVO save(OverflowList overflowList, String overflowListGoodsStr);
    //报溢单查询
    Map<String, Object> list(String sTime, String eTime);
    //查询报溢单商品信息
    Map<String, Object> goodsList(Integer overflowListId);
}
