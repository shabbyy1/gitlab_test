package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

public interface GoodsTypeService {
    //新增商品分类
    ServiceVO save(String goodsTypeName, Integer pId);
    //删除商品分类
    ServiceVO delete(Integer goodsTypeId);
    //三级分类
    String loadGoodsType();
}
