package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    //分页查询供应商
    Map<String, Object> list(Integer page, Integer rows, String supplierName);
    //供应商添加或修改
    ServiceVO save(Supplier supplier);
    ////删除供应商（支持批量删除）
    ServiceVO delete(String ids);
}
