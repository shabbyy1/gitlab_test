package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.OverflowDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowServiceImpl implements OverflowService {

    @Autowired(required = false)
    private OverflowDao overflowDao;

    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {

        overflowDao.saveOverflowList(overflowList);
        String overflowNumber = overflowList.getOverflowNumber();
        Integer overflowListId= overflowDao.getOverflowListIdByNumber(overflowNumber);


        //关联
        List<OverflowListGoods> overflowListGoodsList = JSONObject.parseArray(overflowListGoodsStr, OverflowListGoods.class);
        for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
            overflowListGoods.setOverflowListId(overflowListId);

            overflowDao.saveOverflowListGoods(overflowListGoods);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//报溢单查询
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowLists=overflowDao.list(sTime, eTime);
        map.put("rows",overflowLists);

        return map;
    }

    @Override//报溢单商品信息
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList=overflowDao.goodsList(overflowListId);

        map.put("rows",overflowListGoodsList);
        return map;
    }
}
