package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.GoodTypeTree;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired(required = false)
    private GoodsTypeDao goodsTypeDao;


    @Override//新增商品分类
    public ServiceVO save(String goodsTypeName, Integer pId) {
        if (pId == 1 || pId == -1) {
            goodsTypeDao.save1(goodsTypeName, pId);
        }else {
            goodsTypeDao.save2(goodsTypeName, pId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//删除商品分类
    public ServiceVO delete(Integer goodsTypeId) {
        goodsTypeDao.delete(goodsTypeId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//三级分类
    public String loadGoodsType() {
        List<GoodsType> goodsTypeList = goodsTypeDao.loadGoodsType();
        List<GoodTypeTree> typeTrees = new ArrayList<>();//一级
        List<GoodTypeTree> goodsTypeTreeList = new ArrayList<>();//全分类
        for (GoodsType goodsType : goodsTypeList) {
            GoodTypeTree goodTypeTree = translation(goodsType);
            goodsTypeTreeList.add(goodTypeTree);
        }
        for (GoodTypeTree goodTypeTree : goodsTypeTreeList) {
            if (goodTypeTree.getPId() == -1) {
                typeTrees.add(findChildren(goodTypeTree, goodsTypeTreeList));
            }
        }
        return JSONObject.toJSONString(typeTrees);
    }

    private GoodTypeTree findChildren(GoodTypeTree goodTypeTree, List<GoodTypeTree> goodsTypeTreeList) {
        goodTypeTree.setChildren(new ArrayList<>());
        for (GoodTypeTree goodType : goodsTypeTreeList) {
            if (goodTypeTree.getId().equals(goodType.getPId())) {
                if (goodTypeTree.getChildren() == null) {
                    goodTypeTree.setChildren(new ArrayList<>());
                }
                goodTypeTree.getChildren().add(findChildren(goodType, goodsTypeTreeList));
            }
        }
        return goodTypeTree;
    }

    //转化
    private GoodTypeTree translation(GoodsType goodsType) {
        GoodTypeTree goodTypeTree = new GoodTypeTree();
        goodTypeTree.setId(goodsType.getGoodsTypeId());
        goodTypeTree.setText(goodsType.getGoodsTypeName());
        Integer typeState = goodsType.getGoodsTypeState();
        if (typeState != null) {
            goodTypeTree.setState((typeState == 0) ? "open" : "closed ");
        }
        goodTypeTree.setIconCls("goods-type");
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("state", typeState);
        goodTypeTree.setAttributes(hashMap);
        goodTypeTree.setPId(goodsType.getPId());
        return goodTypeTree;
    }
}
