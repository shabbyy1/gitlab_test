package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired(required = false)
    private SupplierDao supplierDao;

    @Autowired
    private LogService logService;

    @Override//分页查询商品库存信息
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> supplierList = supplierDao.getSupplierList(offSet, rows, supplierName);

        int total = supplierList.size();
        map.put("total", total);
        map.put("rows", supplierList);
        return map;
    }

    @Override//供应商添加或修改
    public ServiceVO save(Supplier supplier) {
        // 角色ID为空时，说明是新增操作，需要先判断角色名是否存在
        if (supplier.getSupplierId() == null) {

            Supplier exSupplier = supplierDao.findSupplierByName(supplier.getSupplierName());

            if (exSupplier != null) {
                return new ServiceVO<>(ErrorCode.ROLE_EXIST_CODE, ErrorCode.ROLE_EXIST_MESS);
            }

            logService.save(new Log(Log.INSERT_ACTION, "新增角色:" + supplier.getSupplierName()));
            supplierDao.insertSupplier(supplier);

        } else {

            logService.save(new Log(Log.UPDATE_ACTION, "修改角色:" + supplier.getSupplierName()));

            supplierDao.updateSupplier(supplier);

        }

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//删除
    public ServiceVO delete(String ids) {
        String[] deleteIds = ids.split(",");
        for (String deleteId : deleteIds) {
            supplierDao.deleteBySupplierId(deleteId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }


}
