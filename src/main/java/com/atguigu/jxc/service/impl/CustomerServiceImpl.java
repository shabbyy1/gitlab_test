package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired(required = false)
    private CustomerDao customerDao;

    @Override//客户列表分页（名称模糊查询）
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> customerList = customerDao.getCustomerList(offSet, rows, customerName);

        int total = customerList.size();
        map.put("total", total);
        map.put("rows", customerList);
        return map;
    }

    @Override//新增或修改角色
    public ServiceVO save(Customer customer) {
        // 角色ID为空时，说明是新增操作，需要先判断角色名是否存在
        if (customer.getCustomerId() == null) {

            Customer exCustomer = customerDao.findCustomerByName(customer.getCustomerName());

            if (exCustomer != null) {
                return new ServiceVO<>(ErrorCode.ROLE_EXIST_CODE, ErrorCode.ROLE_EXIST_MESS);
            }

            customerDao.insertCustomer(customer);

        } else {

            customerDao.updateCustomer(customer);

        }

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//批量删除角色
    public ServiceVO delete(String ids) {
        String[] deleteIds = ids.split(",");
        for (String deleteId : deleteIds) {
            customerDao.deleteByCustomerId(deleteId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }
}
