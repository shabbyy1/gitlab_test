package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.DamageDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageServiceImpl implements DamageService {

    @Autowired(required = false)
    private DamageDao damageDao;





    @Override//保存报损单
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {

        damageDao.saveDamageList(damageList);
        //此时damageListId为null
        String damageNumber = damageList.getDamageNumber();
        Integer damageListId = damageDao.getDamageListIdByNumber(damageNumber);


       //关联
        List<DamageListGoods> damageListGoodsList = JSONObject.parseArray(damageListGoodsStr, DamageListGoods.class);
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageListId);
            damageDao.saveDamageListGoods(damageListGoods);
        }

        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//报损单查询
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageLists=damageDao.list(sTime, eTime);
        map.put("rows",damageLists);

        return map;
    }

    @Override//查询报损单商品信息
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageLists=damageDao.goodsList(damageListId);

        map.put("rows",damageLists);
        return map;
    }
}
