package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.Role;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.sun.corba.se.impl.oa.toa.TOA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired(required = false)
    private GoodsDao goodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override//分页查询商品库存信息
    public Map<String, Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsList(offSet, rows, codeOrName, goodsTypeId);

        int total = goodsList.size();
        map.put("total", total);
        map.put("rows", goodsList);
        return map;
    }

    @Override//分页查询商品信息
    public Map<String, Object> listGoods(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsAllList = goodsDao.listGoods(offSet, rows, goodsName, goodsTypeId);

        int total = goodsAllList.size();
        map.put("total", total);
        map.put("rows", goodsAllList);
        return map;
    }

    @Override//添加或修改商品信息
    public ServiceVO save(Goods goods) {
        // 商品id为空，新增
        if (goods.getGoodsId() == null) {

            Goods exGoods = goodsDao.findGoodsByName(goods.getGoodsName());

            if (exGoods != null) {
                return new ServiceVO<>(ErrorCode.REQ_ERROR_CODE, ErrorCode.REQ_ERROR_MESS);
            }
            if (goods.getInventoryQuantity() == null) {
                goods.setInventoryQuantity(0);
            }
            if (goods.getState() == null) {
                goods.setState(0);
            }

            goodsDao.insertGoods(goods);

        } else {

            goodsDao.updateGoods(goods);

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//删除商品信息
    public ServiceVO delete(Integer goodsId) {
        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        if (goods.getState() == 1) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        }
        if (goods.getState() == 2) {
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }else {
            goodsDao.delete(goodsId);
            return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

        }
    }

    @Override//分页查询无库存商品信息
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> noInventoryQuantityList = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);

        int total = noInventoryQuantityList.size();
        map.put("total", total);
        map.put("rows", noInventoryQuantityList);
        return map;
    }

    @Override//分页查询有库存商品信息
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();

        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> hasInventoryQuantityList = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);

        int total = hasInventoryQuantityList.size();
        map.put("total", total);
        map.put("rows", hasInventoryQuantityList);
        return map;
    }

    @Override//添加商品期初库存
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {

        goodsDao.saveStock(goodsId, inventoryQuantity, purchasingPrice);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//删除商品库存
    public ServiceVO deleteStock(Integer goodsId) {

        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        if (goods.getState() == 1) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        }
        if (goods.getState() == 2) {
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }else {
            goodsDao.deleteStock(goodsId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    @Override//查询所有 当前库存量 小于 库存下限的商品信息
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();
        List<Goods> listAlarmList = goodsDao.listAlarm();
        map.put("rows",listAlarmList);
        return map;
    }


}
