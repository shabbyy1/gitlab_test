package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();

    //分页查询商品库存信息
    Map<String, Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);
    //分页查询商品信息
    Map<String, Object> listGoods(Integer page, Integer rows, String goodsName, Integer goodsTypeId);
    //添加或修改商品信息
    ServiceVO save(Goods goods);
    //删除商品信息
    ServiceVO delete(Integer goodsId);
    //分页查询无库存商品信息
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);
    //分页查询有库存商品信息
    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);
    //添加商品期初库存
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);
    //删除商品库存
    ServiceVO deleteStock(Integer goodsId);
    //查询所有 当前库存量 小于 库存下限的商品信息
    Map<String, Object> listAlarm();
}
