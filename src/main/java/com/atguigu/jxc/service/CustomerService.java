package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    //客户列表分页（名称模糊查询）
    Map<String, Object> list(Integer page, Integer rows, String customerName);
    //客户添加或修改
    ServiceVO save(Customer customer);
    //客户删除（支持批量删除）
    ServiceVO delete(String ids);
}
